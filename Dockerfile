FROM alpine
COPY _opt /opt/emacs
VOLUME /opt/emacs
CMD ["/bin/sh", "-c", "trap 'exit 147' TERM; tail -f /dev/null & wait ${!}"]
